import { Box, Text, Button, Circle, Link } from '@chakra-ui/react';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
// import YoutubeSection from '../components/YoutubeSection';
import Contactme from '../components/Contactme';
import Skills from '../components/Skills';
import About from '../components/About';
import Projects from '../components/Projects';
import Intro from '../components/Intro';

export default function Home({ data }) {
  return (
    <div>
      <Head>
        <title>Iyyappan FullStack develop</title>
        <meta name="description" content="Iyyappan Murugan Portfolio" />
        <link rel="icon" href="/i.jpeg" />
      </Head>
      <Intro />
      <Skills />
      <Projects />
      <About />
      {/* <YoutubeSection /> */}
    </div>
  );
}

export async function getStaticProps(context) {
  return {
    props: {
      data: 'Iyyappan Murugan',
    }, // will be passed to the page component as props
  };
}
