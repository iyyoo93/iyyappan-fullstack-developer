import { Box, Center, Flex, Progress, Text } from '@chakra-ui/react';
import Image from 'next/image';
import React from 'react';
import profilepic from '../asserts/profilepic.png';

function About() {
  return (
    <Box id="about" pt="10vh" pd="10vh">
      <Flex direction="column" justify="center" align="center">
        <Text fontWeight="600" fontSize="2em">
          About Me
        </Text>
        <Text fontWeight="600" fontSize="1.5em" color="red">
          Who Am I !!!
        </Text>
      </Flex>
      <Flex direction={['column', 'row']}>
        <Box w={['100vw', '50vw']} p="10">
          <Image src={profilepic} />
        </Box>
        <Flex
          w={['100vw', '50vw']}
          p="10"
          direction="column"
          justify="center"
          align="center"
        >
          <Text fontWeight="700" fontSize="1.5em">
            I am <span style={{ color: 'red' }}>Iyyappan Murugan</span> and I
            have 8 years of experience with{' '}
            <span style={{ color: 'red' }}>FullStack development.</span>
          </Text>
          <br />
          <Text fontWeight="500" fontSize="1.3em">
            Apart from Technologies I love to play BasketBall, Shout the songs
            incorrectly 😂 😂 😂.
          </Text>
        </Flex>
      </Flex>
    </Box>
  );
}

export default About;
