export const LinkMap = [
  {
    path: '',
    linkName: 'Home',
  },
  {
    path: '#about',
    linkName: 'About',
  },
  {
    path: '#projects',
    linkName: 'Projects',
  },
  {
    path: '#skills',
    linkName: 'Skills',
  },
  // {
  //   path: "#youtubechannel",
  //   linkName: "Videos",
  // },
];
