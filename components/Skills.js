import { Box, Flex, Progress, Text } from "@chakra-ui/react";
import React from "react";

const skills = [
  { tech: "React", level: "95" },
  { tech: "Redux", level: "90" },
  { tech: "Next.js", level: "95" },
  { tech: "Javascript", level: "90" },
  { tech: "Html", level: "90" },
  { tech: "CSS", level: "80" },
  { tech: "Java", level: "60" },
  { tech: "Jenkins", level: "60" },
  { tech: "Docker", level: "60" },
];
function Skills() {
  return (
    <Box id="skills" pt="10vh" pb="10vh">
      <Flex direction="column" justify="center" align="center">
        <Text fontWeight="600" fontSize="2em"></Text>
        <Text fontWeight="600" fontSize="2em" color="red">
          SKILLS{" "}
        </Text>
      </Flex>
      <Flex direction={["column", "row"]}>
        <Box w={["100vw", "50vw"]} p="10">
          <Text fontWeight="600" fontSize="1.2em">
            I am a
          </Text>
          <Text fontWeight="300" fontSize="1.2em">
            Fullstack Developer who loves to work in multiple trending
            technologies like React.js, Next.js, Redux from frontend and Node,
            Java for backend I have my handson CI/CD to the application,
            currently trying to learn AWS to improve myself. Enjoy building
            websites...
          </Text>
        </Box>
        <Box w={["100vw", "50vw"]} p="10">
          {skills.map(({ tech, level }) => (
            <>
              <Flex direction="row" justify="space-between">
                <Text>{tech}</Text>
                <Text>{level}%</Text>
              </Flex>
              <Progress colorScheme="facebook" hasStripe value={level} />
            </>
          ))}
        </Box>
      </Flex>
    </Box>
  );
}

export default Skills;
