import { CalendarIcon } from "@chakra-ui/icons";
import {
  Box,
  Center,
  Flex,
  Progress,
  Text,
  Link,
  AspectRatio,
} from "@chakra-ui/react";
import Image from "next/image";
import React from "react";

function ProjectsSection({ project }) {
  const { title, demoLink, gitLink } = project;
  return (
    <Box h="100vh" w="80vw" m="5" border="4px solid white">
      <Center fontWeight={600} h="10vh">
        {title}
      </Center>
      <AspectRatio ratio={1} h="75vh">
        <iframe title={title} src={demoLink} />
      </AspectRatio>
      <Flex direction="column" h="15vh" mt="1" justify="center" align="center">
        <Link zIndex={100} href={gitLink} isExternal>
          Click to view Source code
        </Link>
        <Link zIndex={100} href={demoLink} isExternal>
          Click to see Demo
        </Link>
      </Flex>
    </Box>
  );
}

export default ProjectsSection;
