import { Box, Text } from "@chakra-ui/react";
import React from "react";
import styles from "../styles/Home.module.css";

function Footer() {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      bg="grey"
      h="8vh"
      className={styles.footer}
    >
      <Text fontSize="sm">
        Love software development | STAY HUNGRY TO LEARN
      </Text>
    </Box>
  );
}

export default Footer;
