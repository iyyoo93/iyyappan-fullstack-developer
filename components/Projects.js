import { CalendarIcon } from "@chakra-ui/icons";
import {
  Box,
  Center,
  Flex,
  Progress,
  Text,
  Link,
  AspectRatio,
} from "@chakra-ui/react";
import Image from "next/image";
import React from "react";
import ProjectsSection from "./ProjectSection";

const projects = [
  {
    title: "Event Manager",
    demoLink: "https://event-manager-4553b.web.app/",
    gitLink: "https://github.com/iyyoo93/EventManger",
  },
  {
    title: "Portfolio",
    demoLink: "https://iyyappan-fullstack-developer.vercel.app/",
    gitLink: "https://gitlab.com/iyyoo93/iyyappan-fullstack-developer",
  },
  {
    title: "Todo app",
    demoLink: "https://iyyoo93.github.io/todo-web-app/",
    gitLink: "https://github.com/iyyoo93/todo-web-app",
  },
];
function Projects() {
  return (
    <Box id="projects" pt="10vh" pd="10vh" bg="black" color="white">
      <Flex direction="column" justify="center" align="center">
        <Text fontWeight="600" fontSize="2em">
          Projects
        </Text>
        <Text fontWeight="600" fontSize="1.5em" color="red">
          What I have done !!!
        </Text>
      </Flex>
      <Center>
        <Flex direction="column" justify="space-between">
          {projects.map((project) => (
            <ProjectsSection key={project.title} project={project} />
          ))}
        </Flex>
      </Center>
    </Box>
  );
}

export default Projects;
