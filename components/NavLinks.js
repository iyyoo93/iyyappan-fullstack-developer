import { Box, Center, Stack, Link, ScaleFade } from "@chakra-ui/react";
import { LinkMap } from "./constants/LinkMap";

const desktopCenterStyle = {
  color: "white",
};

const mobileCenterStyle = {
  bg: "rgb(59, 57, 57)",
  color: "white",
  height: "7vh",
  width: "100%",
  borderBottom: "white solid 1px",
};

function NavLinks({ direction, setNavOpen }) {
  const isMobileView = direction === "column";
  const centerStyle = isMobileView ? mobileCenterStyle : desktopCenterStyle;
  return (
    <Stack
      direction={direction}
      m={0}
      mr={10}
      p={0}
      flexBasis="content"
      w={isMobileView ? "100%" : "70vw"}
      spacing={isMobileView ? "" : "24px"}
      initialScale={0.6}
    >
      {LinkMap.map(({ path, linkName }, index) => (
        <Center
          {...centerStyle}
          key={path}
          _hover={{ color: "red" }}
          borderTop={!index && isMobileView ? "white solid 1px" : ""}
        >
          <Link href={path} onClick={() => setNavOpen && setNavOpen(false)}>
            <b>{linkName}</b>
          </Link>
        </Center>
      ))}
    </Stack>
  );
}

export default NavLinks;
