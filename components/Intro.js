import { Box, Text, Button, Link } from "@chakra-ui/react";
import styles from "../styles/Home.module.css";
import Contactme from "./Contactme";

export default function Intro({ data }) {
  return (
    <main className={styles.main}>
      <Box h="30vh" mt="10vh">
        <Text className={styles.HelloText}>Hello, my name is</Text>
        <Text className={styles.NameText}>
          <b>Iyyappan Murugan</b>
        </Text>
        <Text className={styles.DescText}>
          I am a <span className={styles.NameText}> FullStack </span>Developer
        </Text>
        <Link
          href="https://iyyappan-fullstack-developer.vercel.app/Iyyappan_Resume.pdf"
          download
        >
          <Button border="3px solid skyblue">Download Resume</Button>
        </Link>
      </Box>
      <Contactme />
      <Text className={styles.HomeText}>
        <b>
          Love what you do...
          <br /> Do what you Love...
        </b>
      </Text>
    </main>
  );
}
