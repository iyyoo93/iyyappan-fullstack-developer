import { Box, Center, HStack, Link, AspectRatio, Text } from "@chakra-ui/react";
import Image from "next/image";
import React from "react";
import youtubeChannel from "../asserts/youtubeChannel.png";

const videoIds = [
  "46R1-9E-u-c",
  "5oh162ejPOQ",
  "ZjbyP4ahoZw",
  "CxodV8EDQ3g",
  "LKiLxJuvoxc",
  "Sm0u2omyJlc",
  "y-6V_LblLYE",
  "mDA9c54b_PI",
];

function YoutubeSection() {
  return (
    <Box bg="black" pt="5vh" pb="5vh" pl="2vw" pr="2vw" id="youtubechannel">
      <Center>
        <Text color="white" fontWeight="600" fontSize="2em">
          My Technical Youtube Videos
        </Text>
      </Center>
      <HStack
        direction={"row"}
        spacing="24px"
        overflowX="scroll"
        w="100%"
        p="5"
      >
        {videoIds.map((videoId) => (
          <Box w="100vw" key={videoId}>
            <AspectRatio w="40vw" h="65vh">
              <iframe
                title={`Video ${videoId}`}
                alt={`Video ${videoId}`}
                src={`https://www.youtube.com/embed/${videoId}`}
                allowFullScreen
              />
            </AspectRatio>
          </Box>
        ))}
      </HStack>
      <Box>
        <Box display="flex" alignItems="center" justifyContent="center">
          <Text color="white" fontWeight="600" fontSize="2em">
            Checkout out Tech{" "}
            <Link
              href="https://www.youtube.com/channel/UCT0N7boZl6Aga1tov6THhhw"
              style={{ color: "blue" }}
            >
              Youtube
            </Link>{" "}
            channel
          </Text>
        </Box>
        <Box pb="10" display="flex" alignItems="center" justifyContent="center">
          <Link
            href="https://www.youtube.com/channel/UCT0N7boZl6Aga1tov6THhhw"
            h="20vh"
            w="30vw"
            isExternal
          >
            <Image h="20vh" w="30vw" src={youtubeChannel} objectFit="content" />
          </Link>
        </Box>
      </Box>
    </Box>
  );
}

export default YoutubeSection;
