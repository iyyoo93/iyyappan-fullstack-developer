import { Box, Text, Button, Link, Circle } from "@chakra-ui/react";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import Image from "next/image";
import twitter from "../asserts/twitter.png";
import facebook from "../asserts/facebook.png";
import youtube from "../asserts/youtube.png";
import insta from "../asserts/Insta.png";
import Linkedin from "../asserts/Linkedin.png";

const mediaObj = [
  {
    media: youtube,
    link: "https://www.youtube.com/channel/UCT0N7boZl6Aga1tov6THhhw",
  },
  {
    media: facebook,
    link: "https://facebook.com/iyyappanmurugan",
  },
  {
    media: insta,
    link: "https://instagram.com/iyyoo93",
  },
  {
    media: twitter,
    link: "https://twitter.com/codetamizhacode",
  },
  {
    media: Linkedin,
    link: "https://www.linkedin.com/in/iyyappan-m-43b528143/",
  },
];

function Contactme() {
  return (
    <Box h="30vh" id="contact">
      <Link href="mailto:iyyoo93@gmail.com">
        <Button
          size="md"
          height="48px"
          width="200px"
          border="2px"
          borderColor="green.500"
          color="white"
          colorScheme="#4caf50"
        >
          <b>Contact me</b>
        </Button>
      </Link>
      <Box mt="5" display="flex" gap="2em">
        {mediaObj.map(({ media, link }) => (
          <Box key={link} height="3em" width="3em">
            <Link href={link} isExternal>
              <Image src={media} />
            </Link>
          </Box>
        ))}
      </Box>
    </Box>
  );
}

export default Contactme;
