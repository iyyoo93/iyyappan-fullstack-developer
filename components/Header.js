import {
  Box,
  Flex,
  Stack,
  Link,
  Text,
  useBreakpointValue,
  VStack,
  useBreakpoint,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import NavLinks from "./NavLinks";

function Header() {
  const [isNavOpen, setNavOpen] = useState(false);
  const bpValue = useBreakpoint();
  const isMobile = bpValue === "base";

  return (
    <Box position="fixed" w="100%" zIndex="1020">
      <Flex
        alignItems="center"
        justifyContent="space-between"
        bg="rgb(59, 57, 57)"
        color="white"
        mh="10vh"
        w="100%"
      >
        <Text fontSize={isMobile ? "2xl" : "3xl"} ml="2" color="blue.100">
          <Link href="/" textDecorationLine="none">
            <b>Iyya</b>
            <b style={{ color: "green" }}>ppan </b>
            <b>Muru</b>
            <b style={{ color: "green" }}>gan</b>
          </Link>
        </Text>
        {isMobile ? (
          <Box>
            {!isNavOpen ? (
              <Link>
                <HamburgerIcon
                  h={7}
                  w={7}
                  mr={5}
                  onClick={() => setNavOpen(true)}
                />
              </Link>
            ) : (
              <Link>
                <CloseIcon
                  h={6}
                  w={6}
                  mr={5}
                  onClick={() => setNavOpen(false)}
                />
              </Link>
            )}
          </Box>
        ) : (
          <NavLinks direction="row" initial />
        )}
      </Flex>
      {isNavOpen && isMobile && (
        <NavLinks direction="column" setNavOpen={setNavOpen} />
      )}
    </Box>
  );
}

export default Header;
